<?
	header("Content-type: image/png");
	require_once("perlin.php");

	$height = 200;
	$width = 800;

	$num_major_inflection = 20;
	$persistence = .5;
	$num_iter = 8;

	mt_srand();
	$initial_offset = mt_rand(0,$width * 100);

	$im = imagecreatetruecolor($width,$height);
	$white = imagecolorallocate($im, 255, 255, 255);
	imagefilledrectangle($im, 0, 0, $width, $height, $white);

	$black = imagecolorallocate($im, 0, 0, 0);
	$prev = null;
	for($i=0;$i<$width;$i++) {
		$val = (Perlin(($i/($width / $num_major_inflection)) + $initial_offset, $persistence, $num_iter) + 1) * ($height / 2);
		if($prev != null) imageline($im, $i-1, $prev, $i, $val, $black);
		$prev = $val;
		
	}

	imagepng($im);
?>
