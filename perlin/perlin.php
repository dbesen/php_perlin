<?
	function noise($setnum, $x) {
		mt_srand($setnum + $x * 1000);
		$max = mt_getrandmax() / 2;
		return (mt_rand() / $max) - 1;
	}

	function SmoothedNoise($setnum, $x) {
		return noise($setnum, $x)/2 + (noise($setnum, $x-1) + noise($setnum, $x+1))/4;
	}

	function noise2d($setnum, $x, $y) {
		return noise($setnum, $x * 1000 + $y);
	}

	function Cosine_Interpolate($a, $b, $x) {
		$ft = $x * 3.1415926535897932;
		$f = (1 - cos($ft)) * .5;
		return $a * (1-$f) + $b*$f;
	}

	function Interpolated_Noise($setnum, $x) {
		$intx = floor($x);
		$fx = $x - $intx;
		$v1 = SmoothedNoise($setnum, $intx);
		$v2 = SmoothedNoise($setnum, $intx + 1);
		return Cosine_Interpolate($v1, $v2, $fx);
	}

	function Perlin($x, $persistence, $number_octaves) {
		$total = 0;
		$p = $persistence;
		$n = $number_octaves - 1;

		for($i=0;$i<=$n;$i++) {
			$freq = pow(2,$i);
			$amp = pow($p, $i);

			$total += Interpolated_Noise($i, $x * $freq) * $amp;
		}

		return $total;
	}

?>
