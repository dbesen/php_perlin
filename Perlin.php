<?
	define("PERLIN_LINEAR", 1);
	define("PERLIN_COSINE", 2);
	define("PERLIN_CUBIC", 3);
	class Perlin {
		private $num_octaves;
		private $persistence;
		private $seed;
		private $smooth;
		private $interpolation;

		public function __construct($num_octaves, $persistence, $seed, $smooth = true, $interpolation = PERLIN_LINEAR) {
			$this->num_octaves = $num_octaves;
			$this->persistence = $persistence;
			$this->seed = $seed;
			$this->smooth = $smooth;
			$this->interpolation = $interpolation;
		}

		private function noise($vector) {
			$factor = 1;
			$n = $this->seed;
			foreach($vector as $point) {
				$n += $point * $factor;
				$factor *= 17; // why 17?
			}
			mt_srand($n);
			return ((mt_rand() / mt_getrandmax()) - 0.5) * 2; // this has less resolution than a real float generator would...
		}

		private function smoothed_noise($vector) {
//			if($this->smooth) ...
			return $this->noise($vector); // not implemented yet
		}

		private function linear_interpolate($a, $b, $x) {
			return $a * (1-$x) + $b*$x;
		}

		private function cosine_interpolate($a, $b, $x) {
			$ft = $x * M_PI;
			$f = (1 - cos($ft)) * .5;
			return $a * (1-$f) + $b*$f;
		}

		private function cubic_interpolate($v0, $v1, $v2, $v3, $x) {
			$P = ($v3 - $v2) - ($v0 - $v1);
			$Q = ($v0 - $v1) - $P;
			$R = $v2 - $v0;
			$S = $v1;
			return $P * pow($x, 3) + $Q * pow($x, 2) + $R * $x + $S;
		}

		private function interpolated($vector) {
			return $this->smoothed_noise($vector); // not implemented yet
		}

		public function getPoint() {
			$args = func_get_args();
			$total = 0;
			for($i=0;$i<$this->num_octaves;$i++) {
				$frequency = pow(2, $i);
				$amplitude = pow($this->persistence, $i);
				$adjusted_points = array();
				foreach($args as $a) {
					$adjusted_points[] = $a * $frequency;
				}
				$total += $this->interpolated($adjusted_points) * $amplitude;
			}
			return $total;
		}
	}
?>
